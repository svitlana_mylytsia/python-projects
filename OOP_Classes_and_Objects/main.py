from user import User
from post import Post

app_user_one = User("sm@sm.com", "Svitlana Mi", "my_pwd123", "DevOps Engineer")
app_user_one.get_user_info()
app_user_one.change_job_title("Senior DevOps Engineer")
app_user_one.get_user_info()

new_post = Post("almost a DevOps",app_user_one.name)
new_post.get_post_info()