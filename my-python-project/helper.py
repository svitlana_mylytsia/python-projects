def days_to_units(num_of_days,conversion_unit):
    if conversion_unit == "hours":
        return f"{num_of_days} days are {num_of_days * 24} hours"
    elif conversion_unit == "minutes":
        return f"{num_of_days} days are {num_of_days * 24 * 60} minutes"
    if conversion_unit == "seconds":
        return f"{num_of_days} days are {num_of_days * 24 * 60 * 60} seconds"
    else:
        print("Unsupported unit")


def validate_and_execute(days_and_units_dictionary):
    try:
        user_input_digit = int(days_and_units_dictionary["days"])
        if user_input_digit > 0:
            calculated_value = days_to_units(user_input_digit,days_and_units_dictionary["unit"])
            print(calculated_value)
        elif user_input_digit == 0:
            print("Days can not be zero")
        else:
            print("Do not use negative value!")
    except ValueError:
        print("You should use only digits as an input!")

user_input_message = "Please enter number of days and conversion units:\n"