from datetime import datetime

user_input = input("Enter your deadline and goal separated by colon\n")
input_list = user_input.split(":")
goal = input_list[0]
deadline_date = datetime.strptime(input_list[1], "%d.%m.%Y")
today_date = datetime.today()
time_till = deadline_date - today_date

print(f"Dear User, you have {time_till.days} days till the deadline for your goal to {goal}")

