import requests

response = requests.get("https://api.github.com/users/svitlanamylytsia/repos")
print(response.json())

my_projects = response.json()

for project in my_projects:
    print(f"Project Name: {project.get('name')}\n has URL {project.get('url')}")
