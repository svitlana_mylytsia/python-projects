# Updates the job to Software Engineer
employee = {
  "name": "Tim",
  "age": 30,
  "birthday": "1990-03-10",
  "job": "DevOps Engineer"
}
print(employee)
employee["job"] = "Software Engineer"
print(employee)

# Removes the age key from the dictionary
employee.pop("age")
print(employee)

# Loops through the dictionary and prints the key:value pairs one by one

for key_pair in employee:
  print(f"{key_pair}:{employee.get(key_pair)}")

for key, value in employee.items():
  print(f"{key}:{value}")
