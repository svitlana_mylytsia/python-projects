# print all elements in the list that is equal or higher than 10
my_list = [1, 2, 2, 4, 4, 5, 6, 8, 10, 13, 22, 35, 52, 83]
for element in my_list:
    if element >= 10:
        print(element)

# add all elements equal or higher than 10 to new list and print it
my_list = [1, 2, 2, 4, 4, 5, 6, 8, 10, 13, 22, 35, 52, 83]
list_after_10 = []

for element in my_list:
    if element >= 10:
        list_after_10.append(element)

print(list_after_10)

# ask user to input the number and create the list with elements equal or higher that number
user_number = input("Please enter a number\n")
list_after_user_number = []

for element in my_list:
    if element >= int(user_number):
        list_after_user_number.append(element)

print(list_after_user_number)