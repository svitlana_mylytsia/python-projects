employees = [{
    "name": "Tina",
    "age": 30,
    "birthday": "1990-03-10",
    "job": "DevOps Engineer",
    "address": {
        "city": "New York",
        "country": "USA"
    }
},
    {
        "name": "Tim",
        "age": 35,
        "birthday": "1985-02-21",
        "job": "Developer",
        "address": {
            "city": "Sydney",
            "country": "Australia"
        }
    }]

# Prints out - the name, job and city of each employee using a loop.
# The program must work for any number of employees in the list, not just 2.

for items in employees:
    print(items.get("name"), items.get("job"), items.get("address").get("city"))

# Prints the country of the second employee in the list by accessing it directly without the loop.
print(employees[1].get("address").get("country"))
