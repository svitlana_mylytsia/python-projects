import openpyxl


def take_second(elem):
    return elem[1]


input_file = openpyxl.load_workbook("employees.xlsx")
employee_list = input_file["Sheet1"]
final_list = []

for employee_row in range(2, employee_list.max_row + 1):
    row_list = []
    name = employee_list.cell(employee_row, 1).value
    experience = employee_list.cell(employee_row, 2).value
    row_list = [name, experience]
    final_list.append(row_list)

print(final_list)
final_list.sort(key=take_second,reverse=True)
print(final_list)

wb = openpyxl.Workbook()  # creates a workbook object.
ws = wb.active  # creates a worksheet object.

for row in final_list:
    ws.append(row)  # adds values to cells, each list is a new row.

wb.save('employees_sorted.xlsx')  # save to excel file.






