# Merges these two Python dictionaries into 1 new dictionary.
dict_one = {'a': 100, 'b': 400}
dict_two = {'x': 300, 'y': 200}

new_dictionary = {}
new_dictionary.update(dict_one)
new_dictionary.update(dict_two)
print(new_dictionary)

# Sums up all the values in the new dictionary and print it out
sum_of_values = 0

for item in new_dictionary:
    sum_of_values = sum_of_values + int(new_dictionary.get(item))
print(sum_of_values)

# Prints the max and minimum values of the dictionary
min_value = min(new_dictionary.values())
max_value = max(new_dictionary.values())
print(f"Min value is {min_value}, Max value is {max_value}")
