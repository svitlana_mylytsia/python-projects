# Write a function that accepts a list of dictionaries with employee age and
# prints out the name and age of the youngest employee.


def youngest_employee(list_of_dicts):
    youngest = {}
    for person in list_of_dicts:
        if youngest:
            if youngest["age"] > person["age"]:
                youngest = person
        else:
            youngest = person
    print(f"The youngest person is {youngest['name']} of age {youngest['age']}")


# Write a function that accepts a string and calculates the
# number of upper case letters and lower case letters.


def lower_upper_calculation(my_string):
    upper_letter = 0
    lower_letter = 0

    for i in my_string:
        if i.isupper():
            upper_letter += 1
        else:
            lower_letter += 1
    print(f"String {my_string} contains {upper_letter} upper case letters and {lower_letter} lower case letters")


# Write a function that prints the even numbers from a provided list.
# For cleaner code, declare these functions in its own helper Module and use them in the main.py file


def even_numbers_in_list(input_list):
    even_number = []
    for i in input_list:
        if int(i) % 2 == False:
            even_number.append(i)
    print(even_number)
