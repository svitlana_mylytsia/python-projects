from person import Person


class Student(Person):
    def __init__(self, first_name, last_name, age, lectures):
        super().__init__(first_name, last_name, age)
        self.lectures = lectures

    # can print the full name
    def print_student_full_name(self):
        print(f"Student's full name is {self.first_name} {self.last_name}")

    # can list the lectures, which the student attends
    def list_student_lectures(self):
        print(f"Student {self.first_name} {self.last_name} can attend {self.lectures}")

    # can add new lectures to the lectures list (attend a new lecture)
    def add_student_lectures(self, new_lectures):
        self.lectures.append(new_lectures)
        print(f"{new_lectures} is added. "
              f"A new list of student {self.first_name} {self.last_name} lectures is {self.lectures}")

    # can remove lectures from the lectures list (leave a lecture)
    def remove_student_lectures(self, removed_lecture):
        self.lectures.remove(removed_lecture)
        print(f"{removed_lecture} is removed. "
              f"A new list of student {self.first_name} {self.last_name} lectures is {self.lectures}")
