import random
import sys

random_number = random.randint(1,9)

while True:
    user_input = int(input("Please guess the number between 1 and 9\n"))
    if user_input == random_number:
        print(f"YOU WON! The number is {random_number}")
        sys.exit()
    elif user_input < random_number:
        print("Your number is too low!")
    elif user_input > random_number:
        print("Your number is too high!")