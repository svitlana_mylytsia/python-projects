import sys


def numbers_validation(my_number):
    if my_number.isdigit():
        my_digit = int(my_number)
        return my_digit
    else:
        print("You should use only digits")


def calculation(first_num, second_num, operation):
    if operation == "plus":
        res = first_num + second_num
        return res
    elif operation == "minus":
        res = first_num - second_num
        return res
    elif operation == "multiply":
        res = first_num * second_num
        return res
    elif operation == "divide":
        res = first_num / second_num
        return res
    else:
        print("You have used invalid operation")


user_input = ""
number_of_attempts = 0

while user_input != "exit":
    user_input = input("Please enter parameters in the format: first_number:operation:second_number\n")
    if user_input == "exit":
        print(f"Number of attempts is {number_of_attempts}")
        sys.exit()
    else:
        number_of_attempts += 1
        my_list = user_input.split(":")
        operation = my_list[1]
        first_number = numbers_validation(my_list[0])
        second_number = numbers_validation(my_list[2])

        if first_number and second_number:
            result_of_calculation = calculation(first_number, second_number, operation)

            if result_of_calculation:
                print(f"Result of calculation is {result_of_calculation}")
