from student import Student
from professor import Professor
from lecture import Lecture

student_one = Student("Svitlana", "Mi", "33", ["Math", "Programming", "DevOps"])
student_two = Student("Artem", "Mimi", "32", ["Math", "Programming"])

professor_one = Professor("Artem", "Mylytsia", "33", ["Programming", "DevOps"])
lecture_one = Lecture("OOP", "60", "120", ["Artem Mi", "Svit Mimi"])

student_one.print_student_full_name()
student_two.print_student_full_name()
print("------------------------------")
student_one.list_student_lectures()
student_two.list_student_lectures()
print("------------------------------")
student_one.add_student_lectures("OOP")
student_two.add_student_lectures("DevOps")
print("------------------------------")
student_one.remove_student_lectures("Math")
student_two.remove_student_lectures("Programming")
print("------------------------------")
professor_one.print_professor_full_name()
professor_one.list_professor_subjects()
professor_one.add_professor_subjects("OOP")
professor_one.remove_professor_subjects("Programming")
print("------------------------------")
lecture_one.lecture_info()
lecture_one.add_professors_to_lecture("Ivan Pi")
print("------------------------------")
student_one.print_full_name()
professor_one.print_full_name()
