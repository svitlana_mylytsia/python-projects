from functions_helper import youngest_employee, lower_upper_calculation, even_numbers_in_list

employees = [{
    "name": "Tina",
    "age": 30,
    "birthday": "1990-03-10",
    "job": "DevOps Engineer",
    "address": {
        "city": "New York",
        "country": "USA"
    }
},
    {
        "name": "Tim",
        "age": 35,
        "birthday": "1985-02-21",
        "job": "Developer",
        "address": {
            "city": "Sydney",
            "country": "Australia"
        }
    }]


# Write a function that accepts a list of dictionaries with employee age and
# prints out the name and age of the youngest employee.
youngest_employee(employees)

# Write a function that accepts a string and calculates the
# number of upper case letters and lower case letters.

user_input = input("Please enter a string with UPPER case and lower case letters:\n")
lower_upper_calculation(user_input)

# Write a function that prints the even numbers from a provided list.
# For cleaner code, declare these functions in its own helper Module and use them in the main.py file

even_numbers_in_list([1, 2, 2, 4, 4, 5, 6, 8, 10, 13, 22, 35, 52, 83])