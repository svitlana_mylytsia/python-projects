import sys
from datetime import datetime

user_input = input("Please enter your birthday date\n")

birthday_date = datetime.strptime(user_input, "%d.%m.%Y")
today_date = datetime.today()
print(f"Today is {today_date}")

if today_date.month < birthday_date.month:
    next_birthday = datetime(today_date.year, birthday_date.month, birthday_date.day)
elif today_date.month > birthday_date.month:
    next_birthday = datetime(today_date.year + 1, birthday_date.month, birthday_date.day)
elif today_date.month == birthday_date.month:
    if today_date.day < birthday_date.day:
        next_birthday = datetime(today_date.year, birthday_date.month, birthday_date.day)
    elif today_date.day > birthday_date.day:
        next_birthday = datetime(today_date.year + 1, birthday_date.month, birthday_date.day)
    elif today_date.day == birthday_date.day:
        print("Happy birthday!")
        sys.exit()

time_till = next_birthday - today_date
print(f"Your next birthday is {next_birthday}. It is within {time_till}")
