from person import Person

class Professor(Person):
    def __init__(self, first_name, last_name, age, prof_subjects):
        super().__init__(first_name, last_name, age)
        self.prof_subjects = prof_subjects

    # can print the full name
    def print_professor_full_name(self):
        print(f"Professor's full name is {self.first_name} {self.last_name}")

    # can list the subjects they teach
    def list_professor_subjects(self):
        print(f"Professor {self.first_name} {self.last_name} teaches subjects {self.prof_subjects}")

    # can add new subjects to the list
    def add_professor_subjects(self, new_subjects):
        self.prof_subjects.append(new_subjects)
        print(f"{new_subjects} is added. "
              f"A new list of professor {self.first_name} {self.last_name} subjects is {self.prof_subjects}")

    # can remove subjects from the list
    def remove_professor_subjects(self, removed_subject):
        self.prof_subjects.remove(removed_subject)
        print(f"{removed_subject} is removed. "
              f"A new list of professor {self.first_name} {self.last_name} subjects is {self.prof_subjects}")
