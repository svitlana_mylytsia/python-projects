class Lecture:
    def __init__(self, lecture_name, max_students, lecture_duration, professors_list):
        self.lecture_name = lecture_name
        self.max_students = max_students
        self.lecture_duration = lecture_duration
        self.professors_list = professors_list

    # printing the name and duration of the lecture
    def lecture_info(self):
        print(f"Duration of {self.lecture_name} is {self.lecture_duration}")

    # adding professors to the list of professors giving this lecture
    def add_professors_to_lecture(self, new_professor):
        self.professors_list.append(new_professor)
        print(f"New list of professors for {self.lecture_name} is {self.professors_list}")
