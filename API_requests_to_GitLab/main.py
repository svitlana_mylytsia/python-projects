import requests

response = requests.get("https://gitlab.com/api/v4/users/svitlana_mylytsia/projects")
print(response.json())

my_projects = response.json()

for project in my_projects:
    print(f"Project Name: {project.get('name')}\n has URL {project.get('web_url')}")