**Python Scripts Repository**

This repository contains a collection of Python scripts that perform various tasks.

**Contents**

API_requests_to_GitLab - script for connecting to GitLab via API

AWS_boto - base of scripts to practice AWS BOTO library usage to programmatically interact with AWS services (EC2, EKS, ECR, VPC, etc)

OOP_Classes_and_Objects	- practicing OOP in Python 

Spreadsheet_project	- project for working with data in spreadsheet (sort, calculate, analyze and sum up the results)

my-python-project - scripts for time units conversion and for calculation the time till the deadline

practice - additional practical tasks
