import boto3
from operator import itemgetter

iam_client = boto3.client('iam')

users_list = iam_client.list_users()['Users']

for user in users_list:
    print(f"User {user['UserName']} was last used on {user['PasswordLastUsed']}")

print("===============")

sorted_users_list = sorted(users_list, key=itemgetter('PasswordLastUsed'), reverse=True)
print(f"The most recently used User is {sorted_users_list[0]['UserName']} with ID {sorted_users_list[0]['UserId']}")