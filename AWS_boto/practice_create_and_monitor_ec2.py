import boto3
import paramiko
import time
import requests
import schedule

ec2_resource = boto3.resource('ec2')
ec2_client = boto3.client('ec2')

instance = ec2_resource.create_instances(
    ImageId='ami-00ad2436e75246bba',
    MinCount=1,
    MaxCount=1,
    InstanceType='t2.micro',
    KeyName='my-ssh-key'
)
instance_id = instance[0].id
print(instance_id)


def get_instance_info():
    instance = ec2_client.describe_instances(
        InstanceIds=[instance_id]
    )['Reservations'][0]['Instances'][0]
    return instance


while True:
    instance_status = get_instance_info()['State']['Name']
    if instance_status == "running":
        print(f"Instance status is {instance_status}.")
        break
    else:
        print(f"Instance status is still {instance_status}. Waiting for initialization...")

time.sleep(10)

commands_to_execute = [
    'sudo yum update -y && sudo yum install -y docker',
    'sudo systemctl start docker',
    'sudo usermod -aG docker ec2-user',
    'sudo docker run -d -p 8080:80 --name nginx nginx'
]


hostname = get_instance_info()['PublicIpAddress']
print(hostname)
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(hostname=hostname, username='ec2-user', key_filename='/Users/sveto4ek/.ssh/my-ssh-key.pem')
for command in commands_to_execute:
    stdin, stdout, stderr = ssh.exec_command(command)
    print(stdout.readlines())

ssh.close()

sg_list = ec2_client.describe_security_groups(
    GroupNames=['default']
)

port_open = False
for permission in sg_list['SecurityGroups'][0]['IpPermissions']:
    print(permission)
    # some permissions don't have FromPort set
    if 'FromPort' in permission and permission['FromPort'] == 8080:
        port_open = True

if not port_open:
    sg_response = ec2_client.authorize_security_group_ingress(
        FromPort=8080,
        ToPort=8080,
        GroupName='default',
        CidrIp='0.0.0.0/0',
        IpProtocol='tcp'
    )

# Scheduled function to check nginx application status and reload if not OK 5x in a row
app_not_accessible_count = 0


def restart_container():
    print('Restarting the application...')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=hostname, username='ec2-user', key_filename='/Users/sveto4ek/.ssh/my-ssh-key.pem')
    stdin, stdout, stderr = ssh.exec_command('docker start nginx')
    print(stdout.readlines())
    ssh.close()
    # reset the count
    global app_not_accessible_count
    app_not_accessible_count = 0

    print(app_not_accessible_count)


def monitor_application():
    global app_not_accessible_count
    try:
        response = requests.get(f"http://{hostname}:8080")
        if response.status_code == 200:
            print('Application is running successfully!')
        else:
            print('Application Down. Fix it!')
            app_not_accessible_count += 1
            if app_not_accessible_count == 5:
                restart_container()
    except Exception as ex:
        print(f'Connection error happened: {ex}')
        print('Application not accessible at all')
        app_not_accessible_count += 1
        if app_not_accessible_count == 5:
            restart_container()
        return "test"


schedule.every(10).seconds.do(monitor_application)

while True:
    schedule.run_pending()

