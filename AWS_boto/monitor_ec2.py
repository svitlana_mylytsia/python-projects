import boto3
import paramiko
import time

instance_id = "i-0123a79693d10d323"

ec2_client = boto3.client('ec2')

def get_instance_info():
    instance = ec2_client.describe_instances(
        InstanceIds=[instance_id]
    )['Reservations'][0]['Instances'][0]
    return instance

ec2_instance = ec2_client.describe_instances(
    InstanceIds=[instance_id]
)['Reservations'][0]['Instances'][0]
instance_status = ec2_instance['State']['Name']
while instance_status !="running":
    print(f"Instance status is still {instance_status} ")
else:
    print(f"Instance changed the status to {instance_status} ")

time.sleep(10)

commands_to_execute = [
    'sudo yum update -y && sudo yum install -y docker',
    'sudo systemctl start docker',
    'sudo usermod -aG docker ec2-user',
    'sudo docker run -d -p 8080:80 --name nginx nginx'
]


hostname = get_instance_info()['PublicIpAddress']
print(hostname)
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(hostname=hostname, username='ec2-user', key_filename='/Users/sveto4ek/.ssh/my-ssh-key.pem')
for command in commands_to_execute:
    stdin, stdout, stderr = ssh.exec_command(command)
    print(stdout.readlines())

ssh.close()

sg_list = ec2_client.describe_security_groups(
    GroupNames=['default']
)

print(sg_list)

port_open = False
for permission in sg_list['SecurityGroups'][0]['IpPermissions']:
    print(permission)
    # some permissions don't have FromPort set
    if 'FromPort' in permission and permission['FromPort'] == 8080:
        port_open = True

if not port_open:
    sg_response = ec2_client.authorize_security_group_ingress(
        FromPort=8080,
        ToPort=8080,
        GroupName='default',
        CidrIp='0.0.0.0/0',
        IpProtocol='tcp'
    )
