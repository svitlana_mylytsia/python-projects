import requests
import smtplib
import os
import paramiko
import digitalocean
import time
import schedule

EMAIL_ADDR = os.environ.get('EMAIL_ADDR')
EMAIL_PASS = os.environ.get('EMAIL_PASS')
DO_TOKEN = os.environ.get('DO_TOKEN')
DO_ID = os.environ.get('DO_ID')


def send_notification(email_msg):
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDR, EMAIL_PASS)
        msg = f"Subject: SITE IS DOWN\n{email_msg}"
        smtp.sendmail(EMAIL_ADDR, EMAIL_ADDR, msg)


def restart_container():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname='46.101.143.6', username='root', key_filename='/Users/sveto4ek/.ssh/id_rsa')
    stdin, stdout, stderr = ssh.exec_command('docker start 65619cfedc2b')
    print(stdout.readlines())
    ssh.close()
    print("Application restarted")


def monitor_application():
    try:
        response = requests.get("http://46.101.143.6:8080/")

        if response.status_code == 200:
            print("App is up and running.")
        else:
            print("App is DOWN. Fix it!")
            # send email to me
            email_msg = f"App returned {response.status_code}!"
            send_notification(email_msg)
            # connect via ssh
            restart_container()

    except Exception as ex:
        print(f"Connection error happened: {ex}")
        email_msg = "App is not accessible"
        send_notification(email_msg)

        # restart server
        print("Rebooting the server...")
        do = digitalocean.DigitalOcean(token=DO_TOKEN)
        droplet = do.get_droplet(DO_ID)
        droplet.reboot()

        # restart container
        print("Waiting until the server reboot...")
        time.sleep(30)

        while True:
            response = os.system("ping -c 1 46.101.143.6")
            if response == 0:
                print("Droplet is available.")
                time.sleep(5)
                restart_container()
                break


schedule.every(10).minutes.do(monitor_application)

while True:
    schedule.run_pending()