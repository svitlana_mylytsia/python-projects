import boto3
from operator import itemgetter

client = boto3.client('ecr')

repositories_list = client.describe_repositories()['repositories']
print("The list of repos:")
for repo in repositories_list:
    print(repo['repositoryName'])

images_list = client.describe_images(
    repositoryName='first-repo'
)['imageDetails']

for image in images_list:
    print(f"Image {image['imageTags']} was uploaded on {image['imagePushedAt']}")

sorted_images = sorted(images_list, key=itemgetter('imagePushedAt'), reverse=True)
print(f"The latest image is {sorted_images[0]['imageTags']}")