import boto3

ec2_client = boto3.client('ec2')

subnets_list = ec2_client.describe_subnets()['Subnets']

for subnet in subnets_list:
    print(subnet['SubnetId'])